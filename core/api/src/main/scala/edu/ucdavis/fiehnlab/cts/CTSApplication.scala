package edu.ucdavis.fiehnlab.cts

import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.{CommandLineRunner, SpringApplication}
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.{CorsRegistry, EnableWebMvc, WebMvcConfigurer}

import java.util

@SpringBootApplication
class CTSApplication extends CommandLineRunner {

  override def run(args: String*): Unit = {
    args -> {
    }
  }
}

object CTSApplication {
  def main(args: Array[String]): Unit = {
    SpringApplication.run(classOf[CTSApplication])
  }
}

@Configuration
@EnableWebMvc
class CTSConfiguration extends WebMvcConfigurer {

  override def configureMessageConverters(converters: util.List[HttpMessageConverter[_]]): Unit = {
    val conv = new MappingJackson2HttpMessageConverter()
    val mapper = JsonMapper.builder()
        .addModule(DefaultScalaModule)
        .addModule(new JavaTimeModule)
        .build()
    conv.setObjectMapper(mapper)
    converters.add(conv)
  }

  @Bean
  def corsConfigurer(): WebMvcConfigurer = {
    new WebMvcConfigurer {
      override def addCorsMappings(registry: CorsRegistry): Unit = {
        super.addCorsMappings(registry)
        registry.addMapping("/**").allowedOrigins("*").allowedMethods("*")
      }
    }
  }
}
