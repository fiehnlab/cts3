package edu.ucdavis.fiehnlab.cts3.utils

import com.typesafe.scalalogging.LazyLogging

/**
 * This class converts a molecular formula in a string of atoms
 */
object FormulaExpander extends LazyLogging {

  def expand(formula: String): String = {
    val r = Seq.empty[String]

    logger.info(s"Input: ${formula}")
    formula.split("(?=\\p{Upper})") foreach { x => {
      def multi = x.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")

      logger.info(s"multi: ${multi.mkString(", ")}")
      //        if (1 == multi.length) {
      //          r :++ multi(0)
      //        } else {
      //          val times = multi(1).asInstanceOf[Int]
                r :++ (multi(0) * multi(1).toInt)
      //        }
    }
    }

    r.sorted.mkString("")
  }
}
