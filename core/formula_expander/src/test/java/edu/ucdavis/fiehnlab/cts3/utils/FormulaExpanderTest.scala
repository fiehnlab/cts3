package edu.ucdavis.fiehnlab.cts3.utils

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class FormulaExpanderTest extends AnyWordSpec with Matchers with LazyLogging {
  "FormulaExpander" should {
    "convert formula into string of atoms" in {
      val formula: Map[String, String] = Map(
        "H" -> "H",
        "N2" -> "NN",
        "CO2" -> "COO",
        "NaCl" -> "ClNa",
        "H2O" -> "HHO",
        "OH2" -> "HHO",
        "C6H12" -> "CCCCCCHHHHHHHHHHHH",
        "C8H10N4O2" -> "CCCCCCCCHHHHHHHHHHNNNNOO",
        "C8H10O2N4" -> "CCCCCCCCHHHHHHHHHHNNNNOO",
        "CH3CH2CH2CH3" -> "CCCCHHHHHHHHHH"
      )

      formula.foreach { item =>
        val res = FormulaExpander.expand(item._1)
        logger.info(s"${item._1} expanded to ${res}")

//        res should equal(item._2)
      }
    }
  }
}
